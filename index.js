var axios = require('axios');
var keys = require('./keys.json');
var event = require('./event_sample.json')



// CONFIGURE
const pendingStatusIDInSnipe = 6
const modelIDInSnipe = 51
const domain = "https://snipe.concentrichx.com"

var config = {
  method: 'post',
  url: domain + '/api/v1/hardware',
  headers: { 
    'Content-Type': 'application/json', 
    'Accept': 'application/json', 
    'Authorization': 'Bearer ' + keys['snipe-key']
  },
  params: {
      status_id: pendingStatusIDInSnipe,
      model_id : modelIDInSnipe
  }

};

async function postToSnipe(event) {
    config.params['notes'] = event.event.description
    config.params['serial'] = event.event.serialNumber
    
    return axios(config)
    .then(function (response) {
      // console.log(JSON.stringify(response.data));
      return response.data
    })
    .catch(function (error) {
      console.log(error);
    });
}

postToSnipe(event)
