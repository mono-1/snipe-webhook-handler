var axios = require('axios');
var keys = require('./keys.json');

// CONFIGURE
const pendingStatusIDInSnipe = 6
const modelIDInSnipe = 51
const domain = "https://snipe.concentrichx.com"

var config = {
  method: 'post',
  url: domain + '/api/v1/hardware',
  headers: { 
    'Content-Type': 'application/json', 
    'Accept': 'application/json', 
    'Authorization': 'Bearer ' + keys['snipe-key']
  },
  params: {
      status_id: pendingStatusIDInSnipe,
      model_id : modelIDInSnipe
  }

};


async function postToSnipe(hook) {
  // console.log(hook)
  const data = JSON.parse(hook);
  config.params['notes'] = data.event.description
  config.params['serial'] = data.event.serialNumber
  
  return axios(config)
  .then(function (response) {
    // console.log(JSON.stringify(response.data));
    return response.data
  })
  .catch(function (error) {
    console.log(error);
  });
}


exports.handler = async (event) => {
  // console.log(event.body)
  var response = await postToSnipe(event.body);
  const r = {
      statusCode: 200,
      // body: JSON.stringify(data.Body.toString('ascii')),
      body: response.toString('ascii')
  };
  return r;
};